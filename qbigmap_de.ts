<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de">
<context>
    <name>CenterDialog</name>
    <message>
        <location filename="centerdialog.cpp" line="27"/>
        <source>Set map center</source>
        <translation>Kartenmitte setzen</translation>
    </message>
    <message>
        <location filename="centerdialog.cpp" line="30"/>
        <source>Longitude</source>
        <translation>Geogr. Länge</translation>
    </message>
    <message>
        <location filename="centerdialog.cpp" line="38"/>
        <source>Latitude</source>
        <translation>Geogr. Breite</translation>
    </message>
    <message>
        <location filename="centerdialog.cpp" line="48"/>
        <source>Save as default</source>
        <translation>Als Vorgabe speichern</translation>
    </message>
</context>
<context>
    <name>DelTrkPartDlg</name>
    <message>
        <location filename="deltrkpartdlg.cpp" line="16"/>
        <source>&amp;From position:</source>
        <translation>&amp;Ab Position:</translation>
    </message>
</context>
<context>
    <name>DirTab</name>
    <message>
        <location filename="settingsdialog.cpp" line="49"/>
        <source>&amp;Track directory:</source>
        <translation>&amp;Trackverzeichnis:</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="52"/>
        <location filename="settingsdialog.cpp" line="59"/>
        <location filename="settingsdialog.cpp" line="67"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="56"/>
        <source>&amp;SRTM data directory:</source>
        <oldsource>SRTM data directory:</oldsource>
        <translation>&amp;SRTM-Datenverzeichnis:</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="63"/>
        <source>GPX e&amp;xport directory:</source>
        <translation>GPX-E&amp;xportverzeichnis:</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="64"/>
        <source>Directory for simplified tracks and online maps</source>
        <translation>Verzeichnis vereinfachte Tracks und &amp;Online-Karten:</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="78"/>
        <source>Select track dir</source>
        <translation>Auswahl Track-Verzeichnis</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="85"/>
        <location filename="settingsdialog.cpp" line="92"/>
        <source>Select SRTM dir</source>
        <translation>Wähle SRTM-Datenverzeichnis</translation>
    </message>
</context>
<context>
    <name>GpsTab</name>
    <message>
        <location filename="settingsdialog.cpp" line="9"/>
        <source>Path of gpsbabel</source>
        <translation>Pfad für gpsbabel</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="13"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="15"/>
        <source>GPS device (for gpsbabel):</source>
        <translation>GPS-Gerät (für gpsbabel):</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="19"/>
        <source>GPS interface (for gpsbabel):</source>
        <translation>GPS-Schnittstelle (für gpsbabel):</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="29"/>
        <source>Select gpsbabel</source>
        <translation>Gpsbabel auswählen</translation>
    </message>
</context>
<context>
    <name>GpxListModel</name>
    <message>
        <location filename="gpxlistmodel.cpp" line="28"/>
        <source>POI</source>
        <translation>POI</translation>
    </message>
</context>
<context>
    <name>GpxPointDlg</name>
    <message>
        <location filename="gpxpointdlg.cpp" line="8"/>
        <source>Edit %1</source>
        <translation>Bearbeite %1</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="12"/>
        <source>L&amp;ongitude:</source>
        <translation>Geogr. &amp;Länge:</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="21"/>
        <source>L&amp;atitude:</source>
        <translation>Geogr. &amp;Breite:</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="30"/>
        <source>&amp;Elevation (m):</source>
        <translation>&amp;Höhe (m):</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="35"/>
        <source>SRTM elevation:</source>
        <translation>SRTM-Höhe:</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="41"/>
        <source>&amp;Symbol:</source>
        <translation>&amp;Symbol:</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="47"/>
        <source>&amp;Name:</source>
        <translation>&amp;Name:</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="54"/>
        <source>&amp;Description:</source>
        <translation>&amp;Beschreibung:</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="60"/>
        <source>&amp;Link:</source>
        <translation>&amp;Link:</translation>
    </message>
    <message>
        <location filename="gpxpointdlg.cpp" line="133"/>
        <source>Enter nothing or at least Name and Symbol!</source>
        <translation>Alles freilassen oder mindestens Name und Symbol setzen!</translation>
    </message>
</context>
<context>
    <name>IconTableModel</name>
    <message>
        <location filename="settingsdialog.cpp" line="265"/>
        <source>Select Icon file for Map drawing</source>
        <translation>Wahl des Symbols für die Kartenanzeige</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="267"/>
        <source>Reset to internal icon file</source>
        <translation>Rücksetzen auf Vorgabesymbol</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="295"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="297"/>
        <source>Map Symbol</source>
        <translation>Kartensymbol</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="299"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="301"/>
        <source>Reset</source>
        <translation>Rücksetzen</translation>
    </message>
</context>
<context>
    <name>LayersDialog</name>
    <message>
        <location filename="layersdialog.cpp" line="42"/>
        <source>Add entry</source>
        <translation>Eintrag hinzufügen</translation>
    </message>
    <message>
        <location filename="layersdialog.cpp" line="43"/>
        <source>Delete entry</source>
        <translation>Eintrag löschen</translation>
    </message>
    <message>
        <location filename="layersdialog.cpp" line="44"/>
        <source>Move up</source>
        <translation>Noch oben verschieben</translation>
    </message>
</context>
<context>
    <name>LayersModel</name>
    <message>
        <location filename="layersmodel.cpp" line="50"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="layersmodel.cpp" line="52"/>
        <source>URL</source>
        <translation>URL</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>&amp;Tile size...</source>
        <translation type="obsolete">&amp;Kachelgröße...</translation>
    </message>
    <message>
        <source>POIs</source>
        <translation type="obsolete">POIs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="141"/>
        <source>Photos</source>
        <translation>Fotos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="151"/>
        <source>+/-</source>
        <translation>+/–</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="152"/>
        <source>Time Offet between track and photos</source>
        <translation>Zeitdifferenz Track-Fotos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="167"/>
        <source>&amp;Print...</source>
        <translation>&amp;Drucken...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="170"/>
        <source>Save pixmap...</source>
        <translation>Bild speichern...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="172"/>
        <source>Quit</source>
        <translation>Beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="173"/>
        <source>Read GPX file...</source>
        <translation>GPX-Datei lesen…</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="175"/>
        <source>Save GPX file ...</source>
        <translation>GPX-Datei speichern…</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="176"/>
        <location filename="mainwindow.cpp" line="884"/>
        <source>Read GPS device</source>
        <translation>GPS-Gerät auslesen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="178"/>
        <source>Load track...</source>
        <translation>Track laden...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="180"/>
        <source>Save...</source>
        <translation>Speichern...</translation>
    </message>
    <message>
        <source>Move Track Point</source>
        <translation type="obsolete">Trackpunkt verschieben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="185"/>
        <location filename="mainwindow.cpp" line="1018"/>
        <source>Delete track</source>
        <translation>Track löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="186"/>
        <source>First Track position</source>
        <translation>Trackanfang</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Last Track position</source>
        <translation>Trackende</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="189"/>
        <source>Increment Track position</source>
        <translation>Nächste Trackposition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="191"/>
        <source>Fast increment track position</source>
        <translation>Schnell vorwärts</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="193"/>
        <source>Decrement Track position</source>
        <translation>Vorige Trackposition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="195"/>
        <source>Fast decrement Track positiion</source>
        <translation>Schnell rückwärts</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="197"/>
        <source>Show Track Box</source>
        <translation>Track-Bounding-Box zeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="200"/>
        <source>Simplify track...</source>
        <translation>Track vereinfachen...</translation>
    </message>
    <message>
        <source>&amp;Edit track position...</source>
        <translation type="obsolete">&amp;Bearbeite Trackposition...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="204"/>
        <source>&amp;Delete track position</source>
        <translation>&amp;Lösche Trackposition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="206"/>
        <source>Delete track part...</source>
        <translation>Trackabschnitt entfernen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="208"/>
        <source>&amp;Redraw</source>
        <translation>&amp;Neu darstellen</translation>
    </message>
    <message>
        <source>Tile Position</source>
        <translation type="obsolete">Kachelposition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="210"/>
        <source>Zoom in</source>
        <translation>Vergrößern</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="214"/>
        <source>Zoom out</source>
        <translation>Verkleinern</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="218"/>
        <source>New waypoint</source>
        <translation>Neuer Wegpunkt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="221"/>
        <source>New route point</source>
        <translation>Neuer Routenpunkt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Move Point</source>
        <translation>Punkt verschieben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="227"/>
        <source>Delete route point</source>
        <translation>Routenpunkt löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="231"/>
        <source>Move route point</source>
        <translation>Routenpunkt verschieben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="235"/>
        <source>Edit route point</source>
        <translation>Routenpunkt bearbeiten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="238"/>
        <source>Insert route point</source>
        <translation>Routenpunkt einfügen</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="obsolete">Speichern unter...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="243"/>
        <source>Delete complete route</source>
        <translation>Route komplett löschen</translation>
    </message>
    <message>
        <source>Show photos...</source>
        <translation type="obsolete">Fotos anzeigen...</translation>
    </message>
    <message>
        <source>Hide photos</source>
        <translation type="obsolete">Fotos ausblenden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="260"/>
        <source>Show grid</source>
        <translation>Raster anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="261"/>
        <source>Show/hide distance grid</source>
        <translation>Ein-/Ausblenden des Entfernungsrasters</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="264"/>
        <source>Tile bounds</source>
        <translation>Kachelgrenzen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="265"/>
        <source>Show/hide tile bounds</source>
        <translation>Ein-/Ausblenden der Kachelgrenzen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="400"/>
        <source>&amp;Route/Waypoint</source>
        <translation>&amp;Route/Wegpunkt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="423"/>
        <source>&amp;Photos</source>
        <translation>&amp;Fotos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="848"/>
        <source>(%1, %2)</source>
        <translation>(%1, %2)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="885"/>
        <source>GPX files (*.gpx)</source>
        <translation>GPX-Dateien (*.gpx)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="904"/>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Cannot download track</source>
        <translation>Track kann nicht geladen werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="908"/>
        <source>Cannot download track %1: %2</source>
        <translation>Track %1 kann nicht geladen werden: %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="929"/>
        <source>Load GPX file</source>
        <translation>GPX-Datei laden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="930"/>
        <source>GPX file (*.gpx)</source>
        <translation>GPX-Datei (*.gpx)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="104"/>
        <location filename="mainwindow.cpp" line="252"/>
        <source>Track POIs</source>
        <translation>Track POIs</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="101"/>
        <source>Profile</source>
        <translation>Höhenprofil</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="182"/>
        <source>Select Track Position</source>
        <translation>Trackposition wählen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="202"/>
        <source>&amp;Edit track point...</source>
        <translation>Trackpunkt &amp;bearbeiten...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Save Route ...</source>
        <translation>Route speichern ...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="245"/>
        <source>Open photos...</source>
        <translation>Fotos öffnen ...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="247"/>
        <source>Show photo list</source>
        <translation>Fotoliste anzeigen</translation>
    </message>
    <message>
        <source>Add waypoints</source>
        <translation type="obsolete">Wegpunkte hinzufügen</translation>
    </message>
    <message>
        <source>Add waypoints to track?</source>
        <translation type="obsolete">Wegpunkte in Track aufnehmen?</translation>
    </message>
    <message>
        <source>GPX track (*.gpx)</source>
        <translation type="obsolete">GPX Track (*.gpx)</translation>
    </message>
    <message>
        <source>Save track</source>
        <translation type="obsolete">Track speichern</translation>
    </message>
    <message>
        <source>qbigmap - %1</source>
        <translation type="obsolete">qbigmap - %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1018"/>
        <source>Delete current track?</source>
        <translation>Aktuellen Track löschen?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1106"/>
        <source>Delete track point</source>
        <translation>Trackpunkt löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1107"/>
        <source>Delete the track point (%1,%2) %3</source>
        <translation>Trackpunkt (%1,%2) %3 löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1135"/>
        <source>Save Route %1</source>
        <translation>Speichern der Route %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1136"/>
        <source>Cannot save file %1: %2</source>
        <translation>Datei %1 kann nicht gespeichert werden: %2</translation>
    </message>
    <message>
        <source>Cannot upload route</source>
        <translation type="obsolete">Route kann nicht übertragen werden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1180"/>
        <source>Select photo directory</source>
        <translation>Verzeichnis der Fotos wählen</translation>
    </message>
    <message>
        <source>Download tiles</source>
        <translation type="obsolete">Kacheln werden geladen.</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation type="obsolete">&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="904"/>
        <source>Cannot start gpsbabel</source>
        <translation>Aufruf gpsbabel ist gescheitert.</translation>
    </message>
    <message>
        <source>Cannot upload route %1: %2</source>
        <translation type="obsolete">Route %1 kann nicht übertragen werden: %2</translation>
    </message>
    <message>
        <source>&amp;Tourist</source>
        <translation type="obsolete">&amp;Wanderkarte</translation>
    </message>
    <message>
        <source>&amp;Military</source>
        <translation type="obsolete">&amp;Historische Karte</translation>
    </message>
    <message>
        <source>&amp;Orthophoto</source>
        <translation type="obsolete">&amp;Luftbild</translation>
    </message>
    <message>
        <source>&amp;Hiking</source>
        <translation type="obsolete">&amp;Wanderwege</translation>
    </message>
    <message>
        <source>&amp;Labels</source>
        <translation type="obsolete">&amp;Beschriftung</translation>
    </message>
    <message>
        <source>&amp;Contour</source>
        <translation type="obsolete">&amp;Relief</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="254"/>
        <source>Track Profile</source>
        <translation>Trackprofil</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="268"/>
        <source>Extend to north</source>
        <translation>Nach Norden ausdehnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="270"/>
        <source>Extend to east</source>
        <translation>Nach Osten ausdehnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="272"/>
        <source>Extend to south</source>
        <translation>Nach Süden ausdehnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="274"/>
        <source>Extend to west</source>
        <translation>Nach Westen ausdehnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="276"/>
        <source>Remove from north</source>
        <translation>Im Norden verkürzen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="278"/>
        <source>Remove from east</source>
        <translation>Im Osten verkürzen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="280"/>
        <source>Remove from south</source>
        <translation>Im Süden verkürzen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="282"/>
        <source>Remove from west</source>
        <translation>Im Westen verkürzen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="284"/>
        <source>Move to north</source>
        <translation>Nach Norden verschieben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="286"/>
        <source>Move to east</source>
        <translation>Nach Osten verschieben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="288"/>
        <source>Move to south</source>
        <translation>Nach Süden verschieben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="290"/>
        <source>Move to west</source>
        <translation>Nach Westen verschieben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="292"/>
        <source>Set map center</source>
        <translation>Kartenmitte setzen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="295"/>
        <source>Edit settings...</source>
        <translation>Einstellungen bearbeiten...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="376"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="382"/>
        <source>&amp;Track</source>
        <translation>&amp;Track</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1117"/>
        <source>Delete route</source>
        <translation>Route löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1118"/>
        <source>Delete this route (%1 points)?</source>
        <translation>Diese Route (%1 Punkte) löschen?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1160"/>
        <location filename="mainwindow.cpp" line="1164"/>
        <source>Process failed</source>
        <translation>Prozessstart fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1160"/>
        <source>Cannot start %1</source>
        <translation>Start fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1164"/>
        <source>Process %1 failed: %2</source>
        <translation>Prozesstart %1 fehlgeschlagen: %2</translation>
    </message>
    <message>
        <source>&amp;Route</source>
        <translation type="obsolete">&amp;Route</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="409"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="435"/>
        <source>Tools</source>
        <translation>Werkzeuge</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="obsolete">Seiten</translation>
    </message>
    <message>
        <source>Printout needs %1 pages</source>
        <translation type="obsolete">Ausdruck benötigt %1 Seiten.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>&amp;Type</source>
        <translation>&amp;Kartentyp</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="419"/>
        <source>&amp;Overlays</source>
        <translation>&amp;Ergänzungen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="443"/>
        <source>Track</source>
        <translation>Track</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="690"/>
        <source>Save image</source>
        <translation>Bild speichern</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="691"/>
        <source>Image files (*.png *.jpg)</source>
        <translation>Bilddateien (*.png *.jpg)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="723"/>
        <source>Map print information</source>
        <translation>Druckinformation</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="724"/>
        <source>Tile size: %1 mm
%2x%3 pages.</source>
        <translation>Kachelgröße: %1 mm (%2x%3 Seiten)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="739"/>
        <location filename="mainwindow.cpp" line="744"/>
        <source>Data by www.openstreetmap.org</source>
        <translation>Kartendaten von www.openstreetmap.org</translation>
    </message>
    <message>
        <source>Save settings</source>
        <translation type="obsolete">Einstellungen speichern</translation>
    </message>
    <message>
        <source>Saved new center and zoom</source>
        <translation type="obsolete">Neue Kartenmitte und -vergrößerung wurde gespeichert.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="843"/>
        <source>width: %1 height: %2 zoom: %3</source>
        <translation>Breite: %1 Höhe: %2 Vergrößerung: %3</translation>
    </message>
    <message>
        <source>Tilename: %1_%2_%3</source>
        <translation type="obsolete">Kachelname: %1_%2_%3</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="293"/>
        <source>Edit base layers...</source>
        <translation>Grundebenen bearbeiten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="294"/>
        <source>Edit overlays...</source>
        <translation>Overlays bearbeiten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="428"/>
        <source>&amp;Settings</source>
        <translation>&amp;Einstellungen</translation>
    </message>
    <message>
        <source>(%.05f %.05f)</source>
        <translation type="obsolete">(%.05f %.05f)</translation>
    </message>
</context>
<context>
    <name>MapScene</name>
    <message>
        <location filename="mapscene.cpp" line="301"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Abbrechen</translation>
    </message>
    <message>
        <location filename="mapscene.cpp" line="301"/>
        <source>Download tiles</source>
        <translation>Kacheln werden geladen.</translation>
    </message>
</context>
<context>
    <name>MapView</name>
    <message>
        <location filename="mapview.cpp" line="23"/>
        <source>Select Track Point</source>
        <translation>Trackpunkt wählen</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="24"/>
        <source>Insert Track Point</source>
        <translation>Trackpunkt einfügen</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="25"/>
        <source>Edit Track Point</source>
        <translation>Trackpunkt bearbeiten</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="26"/>
        <source>Delete Track Point</source>
        <translation>Trackpunkt löschen</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="27"/>
        <source>Edit Route Point</source>
        <translation>Routenpunkt bearbeiten</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="28"/>
        <source>Insert Route Point</source>
        <translation>Routenpunkt einfügen</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="29"/>
        <location filename="mapview.cpp" line="368"/>
        <source>Delete Route Point</source>
        <translation>Routenpunkt löschen</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="30"/>
        <source>Edit Waypoint</source>
        <translation>Wegpunkt bearbeiten</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="31"/>
        <location filename="mapview.cpp" line="161"/>
        <source>Delete Waypoint</source>
        <translation>Wegpunkt löschen</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="161"/>
        <source>Delete Waypoint?</source>
        <translation>Wegpunkt löschen?</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="287"/>
        <source>Delete Trackpoint</source>
        <translation>Trackpunkt löschen?</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="288"/>
        <source>Delete trackpoint %1 %2 (%3)?</source>
        <translation>Trackpunt %1 %2 (%3) löschen?</translation>
    </message>
    <message>
        <location filename="mapview.cpp" line="369"/>
        <source>Delete route point %1, %2 (%3)?</source>
        <translation>Routenpunkt %1, %2 (%3) löschen?</translation>
    </message>
    <message>
        <source>Delete route point?</source>
        <translation type="obsolete">Routenpunkt löschen?</translation>
    </message>
</context>
<context>
    <name>OutputSelDlg</name>
    <message>
        <location filename="outputseldlg.cpp" line="7"/>
        <source>Output Map settings</source>
        <translation>Einstellungen Kartenausgabe</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="10"/>
        <source>Include Overlays</source>
        <translation>Overlays einbinden</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="13"/>
        <source>Include Grid</source>
        <translation>Raster einbinden</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="15"/>
        <source>Include Track</source>
        <translation>Track einbinden</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="17"/>
        <source>Include Track Symbols</source>
        <translation>Tracksymbole einfügen</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="19"/>
        <source>Include Route</source>
        <translation>Route einbinden</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="21"/>
        <source>Include Route Symbols</source>
        <translation>Routensymbole einbinden</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="23"/>
        <source>Include Waypoint Symbols</source>
        <translation>Wegpunktsymbole einfügen</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="25"/>
        <source>Restrict Size to track</source>
        <translation>Größe auf Trackausdehnung beschränken</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="27"/>
        <source>&amp;Copyright notice:</source>
        <translation>&amp;Urheberrechtsnotiz:</translation>
    </message>
    <message>
        <location filename="outputseldlg.cpp" line="28"/>
        <source>Map data from OpenStreetMap Contributors</source>
        <translation>Kartendaten von OpenStreetMap-Mitwirkenden</translation>
    </message>
</context>
<context>
    <name>PhotoOffsetDlg</name>
    <message>
        <location filename="photooffsetdlg.cpp" line="12"/>
        <source>&amp;Offset (s):</source>
        <translation>&amp;Offset (s):</translation>
    </message>
</context>
<context>
    <name>PrintTab</name>
    <message>
        <location filename="settingsdialog.cpp" line="114"/>
        <source>&amp;Tile size (mm):</source>
        <oldsource>Tile size (mm):</oldsource>
        <translation>Kachelgröße (mm):</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="121"/>
        <source>Track &amp;width (pt):</source>
        <translation>Trackbreite (pt):</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="128"/>
        <location filename="settingsdialog.cpp" line="141"/>
        <source>Track &amp;color:</source>
        <translation>Trackfarbe:</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="131"/>
        <location filename="settingsdialog.cpp" line="144"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="134"/>
        <source>Route width (pt):</source>
        <translation>Routendicke (pt):</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="173"/>
        <source>Select Track Color</source>
        <translation>Auswahl der Trackfarbe</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="182"/>
        <source>Select Route Color</source>
        <translation>Auswahl der Routenfarbe</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="gpx.cpp" line="32"/>
        <source>trackpoint</source>
        <translation>Trackpunkt</translation>
    </message>
    <message>
        <location filename="gpx.cpp" line="34"/>
        <source>routepoint</source>
        <translation>Routenpunkt</translation>
    </message>
    <message>
        <location filename="gpx.cpp" line="36"/>
        <source>waypoint</source>
        <translation>Wegpunkt</translation>
    </message>
    <message>
        <location filename="gpx.cpp" line="38"/>
        <source>unknown GPX point</source>
        <translation>Unbekannter GPX-Punkttyp</translation>
    </message>
</context>
<context>
    <name>Route</name>
    <message>
        <source>Route</source>
        <translation type="obsolete">Route</translation>
    </message>
</context>
<context>
    <name>SaveRouteDlg</name>
    <message>
        <location filename="saveroutedlg.cpp" line="7"/>
        <source>Save route</source>
        <translation>Route speichern</translation>
    </message>
    <message>
        <location filename="saveroutedlg.cpp" line="8"/>
        <source>&amp;Route name:</source>
        <translation>&amp;Routenname:</translation>
    </message>
    <message>
        <location filename="saveroutedlg.cpp" line="8"/>
        <source>&amp;Track name</source>
        <translation>&amp;Trackname:</translation>
    </message>
    <message>
        <location filename="saveroutedlg.cpp" line="17"/>
        <source>&amp;File name:</source>
        <translation>&amp;Dateiname:</translation>
    </message>
    <message>
        <location filename="saveroutedlg.cpp" line="29"/>
        <source>Select File</source>
        <translation>Datei auswählen</translation>
    </message>
    <message>
        <location filename="saveroutedlg.cpp" line="32"/>
        <source>Add &amp;Waypoints</source>
        <translation>&amp;Wegpunkte hinzufügen</translation>
    </message>
    <message>
        <source>Add &amp;Waypoints to route</source>
        <translation type="obsolete">&amp;Wegpunkte zur Route hinzufügen</translation>
    </message>
    <message>
        <location filename="saveroutedlg.cpp" line="34"/>
        <source>&amp;Upload to GPS device</source>
        <translation>&amp;Zum GPS-Gerät übertragen</translation>
    </message>
    <message>
        <location filename="saveroutedlg.cpp" line="48"/>
        <source>Select Route File</source>
        <translation>Auswahl Routendatei</translation>
    </message>
    <message>
        <location filename="saveroutedlg.cpp" line="49"/>
        <source>GPX file (*.gpx)</source>
        <translation>GPX-Datei (*.gpx)</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="settingsdialog.cpp" line="343"/>
        <source>&amp;GPS Device</source>
        <translation>&amp;GPS-Gerät</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="345"/>
        <source>&amp;Directories</source>
        <translation>&amp;Verzeichnisse</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="349"/>
        <source>&amp;Symbols</source>
        <translation>&amp;Symbole</translation>
    </message>
    <message>
        <source>Track</source>
        <translation type="obsolete">&amp;Track</translation>
    </message>
    <message>
        <location filename="settingsdialog.cpp" line="347"/>
        <source>&amp;Print</source>
        <translation>&amp;Druck</translation>
    </message>
</context>
<context>
    <name>TileSizeDlg</name>
    <message>
        <source>Tile size (mm):</source>
        <translation type="obsolete">Kachelgröße (mm):</translation>
    </message>
</context>
<context>
    <name>TrackExportDlg</name>
    <message>
        <location filename="trackexportdlg.cpp" line="9"/>
        <source>Track File:</source>
        <translation>Trackdatei:</translation>
    </message>
    <message>
        <location filename="trackexportdlg.cpp" line="15"/>
        <source>&amp;OpenStreetMap export</source>
        <translation>&amp;OpenStreetMap-Export</translation>
    </message>
    <message>
        <location filename="trackexportdlg.cpp" line="18"/>
        <source>&amp;Remove timestamp/elevation</source>
        <translation>&amp;Zeit/Höhe entfernen</translation>
    </message>
    <message>
        <location filename="trackexportdlg.cpp" line="21"/>
        <source>Map &amp;title:</source>
        <translation>Karten&amp;titel</translation>
    </message>
    <message>
        <location filename="trackexportdlg.cpp" line="23"/>
        <source>Track</source>
        <translation>Track</translation>
    </message>
    <message>
        <location filename="trackexportdlg.cpp" line="68"/>
        <source>Simple Track File</source>
        <translation>Name des vereinfachten Tracks</translation>
    </message>
    <message>
        <location filename="trackexportdlg.cpp" line="69"/>
        <source>GPX Files (*.gpx)</source>
        <translation>GPX-Dateien (*.gpx)</translation>
    </message>
</context>
<context>
    <name>TrackPosDlg</name>
    <message>
        <location filename="trackposdlg.cpp" line="11"/>
        <source>L&amp;ongitude:</source>
        <translation>Geogr. &amp;Länge:</translation>
    </message>
    <message>
        <location filename="trackposdlg.cpp" line="20"/>
        <source>L&amp;atitude:</source>
        <translation>Geogr. &amp;Breite:</translation>
    </message>
    <message>
        <location filename="trackposdlg.cpp" line="29"/>
        <source>&amp;Elevation:</source>
        <translation>&amp;Höhe:</translation>
    </message>
    <message>
        <location filename="trackposdlg.cpp" line="37"/>
        <source>&amp;Timestamp:</source>
        <translation>&amp;Zeitstempel:</translation>
    </message>
</context>
<context>
    <name>TrackSelModel</name>
    <message>
        <location filename="trackseldialog.cpp" line="59"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="trackseldialog.cpp" line="61"/>
        <source>Start time</source>
        <translation>Startzeit</translation>
    </message>
    <message>
        <location filename="trackseldialog.cpp" line="63"/>
        <source>End time</source>
        <translation>Endzeit</translation>
    </message>
    <message>
        <location filename="trackseldialog.cpp" line="65"/>
        <source>Track points</source>
        <translation>Trackpunkte</translation>
    </message>
</context>
<context>
    <name>TrackSimplifyDlg</name>
    <message>
        <location filename="tracksimplifydlg.cpp" line="15"/>
        <source>Select</source>
        <translation>Auswahl</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="18"/>
        <source>&amp;Failure (m):</source>
        <translation>&amp;Toleranz (m):</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="26"/>
        <source>Track points</source>
        <translation>Trackpunkte</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="30"/>
        <source>&amp;Simple Track file name:</source>
        <translation>Dateiname des vereinfachten Tracks:</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="39"/>
        <source>E&amp;xport</source>
        <translation>E&amp;xport</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="41"/>
        <source>&amp;Replace</source>
        <translation>E&amp;rsetzen</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="116"/>
        <source>File exists</source>
        <translation>Datei vorhanden</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="116"/>
        <source>File %1 exists. Overwrite it?</source>
        <translation>Datei %1 ist bereits vorhanden. Überschreiben?</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="obsolete">Export</translation>
    </message>
    <message>
        <source>&amp;OpenStreetMap</source>
        <translation type="obsolete">&amp;OpenStreetMap</translation>
    </message>
    <message>
        <source>&amp;Google Maps</source>
        <translation type="obsolete">&amp;Google Maps</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="91"/>
        <source>Simple Track File</source>
        <translation>Name des vereinfachten Tracks</translation>
    </message>
    <message>
        <location filename="tracksimplifydlg.cpp" line="92"/>
        <source>GPX Files (*.gpx)</source>
        <translation>GPX-Dateien (*.gpx)</translation>
    </message>
</context>
<context>
    <name>TrackTab</name>
    <message>
        <source>Track directory:</source>
        <translation type="obsolete">Track-Verzeichnis:</translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="obsolete">Auswahl</translation>
    </message>
    <message>
        <source>Select track dir</source>
        <translation type="obsolete">Auswahl Track-Verzeichnis</translation>
    </message>
</context>
</TS>
